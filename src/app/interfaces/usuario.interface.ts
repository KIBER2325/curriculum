export interface UsuarioI{
    usuario:string;
    password:string;
}

export interface UsuarioDataI {
    id:string
    nombre: string;
    apellido: string;
    correo: string;
    celular: number;
    fecha:string;
    hora:string;
    descripcion: string;
  }
  