import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';

import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { UsuarioDataI } from 'src/app/interfaces/usuario.interface';
import { UsuarioService } from 'src/app/services/usuario.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RouteConfigLoadEnd, Router } from '@angular/router';


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit, AfterViewInit {
  
  listaUsuarios: UsuarioDataI[] =[];
 
  displayedColumns: string[] = ['nombre', 'apellido', 'correo', 'celular', 'fecha', 'hora', 'descripcion', 'acciones'];
  dataSource!:  MatTableDataSource<any> ;
  estadoLista!:boolean;
  radio= '';

  @ViewChild(MatPaginator) paginator! : MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private _usuarioService : UsuarioService, private _snackbar :MatSnackBar, private router:Router)  {
  
  }

  ngOnInit(): void {
    console.log(new Date().getMonth());
    this.cargarUsuarios();
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  cargarUsuarios(){
    this.listaUsuarios = this._usuarioService.getUsuario();
    this.dataSource = new MatTableDataSource(this.listaUsuarios);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    
    let a =  localStorage.getItem('dateTime');
    this.radio = a || '';
    console.log(this.listaUsuarios);

  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  eliminarUsuario(usuario:any){
    const opcion = confirm("estas segurisimo de eliminar el dato ?");
    if(opcion){ 
    this._usuarioService.eliminarUsuario(usuario);
    this.cargarUsuarios();
    this._snackbar.open('El dato fue eliminado con exito', '', {
      duration: 1500,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    });
  }
  }


  verUsuario(usuario: any){
    
    this.router.navigate(['contacto/ver-agenda', usuario])
  }
  
    modificarUsuario(usuario:any):void{
      console.log(usuario);
      this.router.navigate(['contacto/crear-agenda', usuario]);

    }
    agregarLike() {
      /*
      EN ESTA CONDICION, ESTAMOS DICIENDO: 
      "SI NO EXISTE LA VARIABLE DE localStorage superheroe"
      REALIZAREMOS LO SIGUIENTE
      */
      
    }
  
    }
  



