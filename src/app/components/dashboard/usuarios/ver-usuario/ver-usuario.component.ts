import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario.service';
@Component({
  selector: 'app-ver-usuario',
  templateUrl: './ver-usuario.component.html',
  styleUrls: ['./ver-usuario.component.css']
})
export class VerUsuarioComponent implements OnInit {
  form!:FormGroup

  constructor(private _activateRoute : ActivatedRoute, private usuarioService: UsuarioService, private router:Router,private fb:FormBuilder) {
    
    this._activateRoute.params.subscribe(params => {
      const id = params['id'];
      console.log(id);
      const usuario = this.usuarioService.buscarUsuario(id);
      console.log(usuario);

      //verifico que la longitud del objeto sea 0
      if(Object.keys(usuario).length === 0){
        this.router.navigate(['/contacto/agenda']);
      }
      this.form = this.fb.group({
        nombre: ['', Validators.required],
        apellido: ['', Validators.required],
        correo: ['', Validators.required],
        celular: ['', Validators.required],
        fecha: ['', Validators.required],
        hora: ['', Validators.required],
        descripcion: ['', Validators.required]
      });

      this.form.patchValue({
        nombre: usuario.nombre,
          apellido: usuario.apellido,
          correo: usuario.correo,
          celular: usuario.celular,
          fecha: usuario.fecha,
          hora: usuario.hora,
          descripcion: usuario.descripcion

      });

      

    });
  }
  volver():void{
    console.log("volvere");
    this.router.navigate(['/contacto/agenda']);
  }

  ngOnInit(): void {
  }
  

}
