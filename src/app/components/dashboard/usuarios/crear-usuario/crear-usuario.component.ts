import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioDataI } from 'src/app/interfaces/usuario.interface';
import { UsuarioService } from 'src/app/services/usuario.service';
@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.css']
})
export class CrearUsuarioComponent implements OnInit {
  idM!:number;
  adicionar = true;
  titulo = 'Agendar ';
  form!:FormGroup;
  fechita!:Date;
 

  constructor(private fb:FormBuilder,
              private _usuarioService:UsuarioService,
              private router : Router, 
              private _snackbar:MatSnackBar,
              private _activeRoute: ActivatedRoute) { 

   this.insertarUsuario();
   this._activeRoute.params.subscribe(params=>{
     this.idM = params['id'];
     console.log(this.idM);
     
     this.form = this.fb.group({
       nombre: ['', Validators.required],
       apellido: ['', Validators.required],
       correo: ['', Validators.required],
       celular: ['', Validators.required],
       fecha: ['', Validators.required],
       descripcion: ['', Validators.required]
    });
    localStorage.setItem('dateTime',this.form.value.fecha);

    this.fechita =  JSON.parse(localStorage.getItem('dateTime') || '{}' );
    //this.fechita = this.form.value.fecha;
      console.log(this.fechita);

     

      if(this.idM.toString() !== 'nuevo'){

       const usuario = this._usuarioService.buscarUsuario(this.idM)
       console.log(usuario);
      
      
       if(Object.keys(usuario).length === 0){
        this.router.navigate(['/contacto/agenda']);
      } 
  
        this.form.patchValue({
          nombre: usuario.nombre,
          apellido: usuario.apellido,
          correo: usuario.correo,
          celular: usuario.celular,
          descripcion: usuario.descripcion

      });
      this.adicionar = false;
      this.titulo = 'Modificar Agenda';
    }
 
  })
  }

  ngOnInit(): void {
  }
  insertarUsuario():void{
    this.form = this.fb.group({
      nombre: ['', Validators.required],
       apellido: ['', Validators.required],
       correo: ['', Validators.required],
       celular: ['', Validators.required],
       fecha: ['', Validators.required],
      
       descripcion: ['', Validators.required]
    });
  }


  agregarUsuario():void{
    if(!this.form.valid){
      return;
    }

    const usuario = this._usuarioService.buscarUsuario(this.idM);

    const user: UsuarioDataI = {
      id : '',
      nombre: this.form.value.nombre,
      apellido: this.form.value.apellido,
      correo: this.form.value.correo,
      celular: this.form.value.celular,
      fecha: this.form.value.fecha,
      hora: this.form.value.fecha,
      descripcion: this.form.value.descripcion
    }

   if(this.adicionar){
    let date= new Date();

    localStorage.setItem('dateTime',this.form.value.fecha);


    user.id = this.makeRandomId();

    this._usuarioService.agregarUsuario(user);

    this.router.navigate(['/contacto/agenda']);
   
    this._snackbar.open('El dato fue agregado con exito', '', {
      duration: 1500,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    });
     }else {
      console.log(user.id);
      
      user.id = usuario.id;
      console.log(user.id);
      user.fecha = usuario.fecha;
      user.hora = usuario.hora;
      this._usuarioService.modificarUsuario(user);

       this.router.navigate(['/contacto/agenda']);

       this._snackbar.open('El comentario fue modificado con exito', '', {
       duration: 1500,
       horizontalPosition: 'center',
       verticalPosition: 'bottom',
       
      });
  }
}
  volver():void{
    console.log("volvere");
    this.router.navigate(['/contacto/agenda']);
  }

  makeRandomId(length = 4) {
    let result = ''
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    for (let i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * characters.length));
   }
   return result;
}
  

}
