import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  form!:FormGroup
  loading:boolean = false;
  constructor(private fb:FormBuilder, private _snackbar: MatSnackBar, private router:Router) { 
  this.formulario();
  }

  ngOnInit(): void {
  }

  formulario():void{
    this.form=this.fb.group({
      nombre:['',[Validators.required,Validators.pattern(/^[a-zA-ZñÑ\s]+$/)]],
      correo:['',[Validators.required,Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      mensaje:['',[Validators.required,Validators.pattern(/^[0-9a-zA-ZñÑ\s]+$/)]],
    })
  }

  fakeLoading():void{
    this.loading = true;
    setTimeout(() => {
      this.loading=false;
    //Redireccionamos al dashboard
      //this.router.navigate(['reportes']);
      this.form.reset();
      this._snackbar.open('Mensaje enviado exitosamente','',{
        duration:5000,
        horizontalPosition:'center',
        verticalPosition:'bottom'
      })
    }, 1500);
  }

   ingresar(){
     console.log(this.form.value);
     

     if (this.form.valid) {
       //Redireccionamos al dashboard
       this.fakeLoading();
       
     } else {
       //mostramos un mensaje de error
       this.error();
       this.form.reset();
     }
  
   }


  error():void{
    this._snackbar.open('usuario o contraseña incorrecto','',{
      duration:5000,
      horizontalPosition:'center',
      verticalPosition:'bottom'
    })
  }

}
