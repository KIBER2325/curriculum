import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { InicioComponent } from './inicio/inicio.component';
import { ReportesComponent } from './reportes/reportes.component';
import { CrearUsuarioComponent } from './usuarios/crear-usuario/crear-usuario.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { VerUsuarioComponent } from './usuarios/ver-usuario/ver-usuario.component';


const routes: Routes = [
  { path: '', component: DashboardComponent,
children:[
  {path:'',component:InicioComponent},
  {path:'agenda',component:UsuariosComponent},
  {path:'cv',component:ReportesComponent},
  { path: 'crear-agenda/:id', component: CrearUsuarioComponent },
  { path: 'ver-agenda/:id', component: VerUsuarioComponent}

  
] }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
