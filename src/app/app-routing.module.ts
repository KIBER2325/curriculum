import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardModule } from './components/dashboard/dashboard.module';
// import { LoginComponent } from './components/login/login.component';

const routes: Routes = [
  // {path:'login',component:LoginComponent},
  {path:'contacto', 
  loadChildren:()=> import('./components/dashboard/dashboard.module').then(x=>x.DashboardModule)
},
{path:'**',pathMatch:'full',redirectTo:'contacto/cv'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
